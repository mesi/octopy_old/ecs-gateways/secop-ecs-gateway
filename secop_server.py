import json
import time
import traceback
import logging
import threading
from time import sleep
from lib.secop.secnode import SecNode
from lib.secop import errors as secop_errors


# TODO:
# Implement Module-wise activates


MANUFACTURER = 'ESS'
SECOP_VERSION = 'v1.0'
SECOP_VERSION_DATE = 'V2019-09-16'
LOG_ID = 'SECoP ECS Gateway: SECoP Server'

class SecopServer(threading.Thread):
    def __init__(self, client_socket, node, set_callback, get_callback):
        """
        "node" is a lib.secop_data.SecNode-object
        "client_socket" is an open socket
        """
        threading.Thread.__init__(self)
        self._socket = client_socket
        self._node = node
        #self._node.accessible_change_callback = self.on_accessible_change
        self._set_callback = set_callback
        self._get_callback = get_callback
        if not isinstance(self._node, SecNode):
            raise TypeError(f"Expected SecNode. Got {self._node.__class__.__name__}")
        self._active = False


    def run(self):
        while True:
            ret = None
            data = self._socket.recv(1024)
            if not data:
                break
            try:
                to_parse = data.decode()
                print(to_parse)
                ret = self.parse(to_parse)
            except UnicodeDecodeError:
                logging.getLogger(LOG_ID).error('not valid input')
            if ret:
                # Send response to client over socket
                self.respond(ret)
        self._socket.close()


    def respond(self, message):
        """Send data to the client ovcer the socket"""
        if isinstance(message, str):
            self._socket.send(message.encode() + b'\n')
        else:
            self._socket.send(json.dumps(message).encode() + b'\n')


    def parse(self, string_to_parse):
        """Parses a string received from the client over the socket"""
        logging.getLogger(LOG_ID).info('Parsing: ' + string_to_parse)
        cmd = string_to_parse.strip("\x0A\x0D").split(' ', 2)
        print(cmd)
        action = cmd[0]
        if len(cmd) > 1:
            specifier = cmd[1]
        if len(cmd) > 2:
            data = cmd[2]
        try:
            if action == 'activate':
                return self.activate()
            elif action == 'change':
                return self.change(specifier, data)
            elif action == 'deactivate':
                return self.deactivate()
            elif action == 'describe':
                return self.describe()
            elif action == 'do':
                return self.do(specifier, data)
            elif action == 'ping':
                return self.ping(specifier)
            elif action == 'read':
                return self.read(specifier)
            elif action == '*IDN?':
                return f"{MANUFACTURER},{self._node.equipment_id},{SECOP_VERSION_DATE},{SECOP_VERSION}"
                # FIXME: Frappy has this hardcoded and refuse to connect if it's anything else!
                #        This obviously must be changed in the future!
                #return f"SINE2020&ISSE,SECoP,{SECOP_VERSION_DATE},{SECOP_VERSION}"
            else:
                return 'error_' + string_to_parse + ' ["ProtocolError", "unknown action", {}]'
        except Exception as err:
            traceback.print_exc()
            return 'error_' + string_to_parse +\
                json.dumps(['InternalError', 'action could not be performed ' + str(err), {}])


    def change(self, acc_id, change_val):
        try:
            accessible = self._node.accessibles[acc_id]
            set_val = self._set_callback(accessible, change_val)
            report = json.dumps(set_val)
            return f"changed {acc_id} {report}"
        except Exception as err:
            return 'error_change {} {}'.format(acc_id, err)


    def activate(self):
        self._active = True
        return 'active'


    def deactivate(self):
        self._active = False
        return 'inactive'


    def describe(self):
        structure = self._node.serialize(pure_secop = True)
        report = json.dumps(structure)
        return f"describing . {report}"


    def do(self, acc_id, argument):
        if not argument:
            argument = 0
        try:
            accessible = self._node.accessibles[acc_id]
        except secop_errors.SecopException as err:
            return 'error_do ' + acc_id + ' ' + str(err)
        if accessible.name == 'stop':
            # On stop we try to set the target to be the same as the current value
            value_accessible = accessible.module().get('value')
            if not value_accessible:
                raise secop_errors.NoSuchParameterError(f"Module {accessible.module().full_name} has no 'value' accessible")
            target_accesible = accessible.module().get('target')
            if not target_accessible:
                raise secop_errors.NoSuchParameterError(f"Module {accessible.module().full_name} has no 'target' accessible")
            current_value = value_accessible.source_value[0]
            target_accessible.set_value = current_value
        try:
            out = accessible.set(argument)
            return 'done ' + acc_id
        except secop_errors.SecopException as err:
            return 'error_do ' + acc_id + ' ' + str(err)


    def ping(self, specifier):
        if not specifier:
            specifier = ''
        reply = 'pong ' + specifier + ' ' + json.dumps([None, {'t': time.time()}])
        return reply


    def read(self, acc_id):
        try:
            accessible = self._node.accessibles[acc_id]
            self._get_callback(accessible)
            if not accessible.has_live_data:
                return 'error_read ' + acc_id + ' no live data available'
            return 'reply ' + acc_id + ' ' + json.dumps(accessible.source_value)
        except secop_errors.SecopException as err:
            return 'error_read ' + acc_id + ' ' + str(err)


    def on_accessible_change(self, accessible):
        if self._active:
            res = 'update ' + accessible.full_id + ' ' + json.dumps(accessible.source_value)
            self.respond(res)
