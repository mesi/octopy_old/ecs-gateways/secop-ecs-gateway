#! /usr/bin/python3
import socket
import time
import sys
from os import path
import json
import logging
import argparse
from time import sleep
from lib.octopyapp.octopyapp import OctopyApp
from secop_server import SecopServer
from lib.secop.secnode import SecNode


APP_ID = 'SECoP ECS Gateway'
LOG_LEVEL = logging.DEBUG
DEFAULT_SOCKET_PORT = 10765


class SECoPECSGateway(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self.subscribe(self.topics['node manager']['structure'], self.on_mqtt_structure_message)
        self.node = None
        self.secop_servers = []
        self.socket_port = DEFAULT_SOCKET_PORT


    def start(self):
        super().start(start_loop = False)
        # Start MQTT main loop in background thread
        self.mqtt_client.loop_start()
        # Don't proceed until we've got a node
        while not self.node:
            logging.warning(f"Waiting for node structure report...")
            sleep(1)
        # Wait a while for readable accessibles to receive values
        tries = 0
        all_accessibles_active = False
        all_accessibles = self.node.accessibles.values()
        while not all_accessibles_active and tries < 10:
            logging.info(f"Waiting for accessibles to receive live data [{'#'*(tries+1)}{'='*(9-tries)}]")
            all_accessibles_active = True
            for accessible in all_accessibles:
                if hasattr(accessible, 'value_topic') and not accessible.has_live_data:
                    # An accessible without live data discovered so the node is not fully alive yet.
                    all_accessibles_active = False
                    logging.debug(f"Accessible {accessible.full_name} is not active")
            tries += 1
            sleep(2)
        if not all_accessibles_active:
            logging.warning(f"Some readable accessibles are missing live data. Continuing anyway but the node may not be fully operational.")
        # Start the SECoP server main loop
        self.main_loop()


    def main_loop(self):
        while True:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                # Open server socket
                sock.bind(('', self.socket_port))
                logging.info("SECoP server socket opened.")
                sock.listen()
                host_name = socket.gethostname()
                host_ip = socket.gethostbyname(host_name)
                logging.info(f"Starting SECoP Server on {host_name} ({host_ip}), port {self.socket_port}.")
                # Listen for clients connecting
                while True:
                    (client_sock, client_addr) = sock.accept()
                    logging.info(f"Client connected ({client_addr})")
                    # Create SECoP socket server object
                    new_client = SecopServer(client_sock,
                                             self.node,
                                             self.on_socket_set_message,
                                             self.on_socket_get_message)
                    new_client.start()
                    self.secop_servers.append(new_client)
                    for client in self.secop_servers.copy():  # Use a copy to avoid problems with the direct editing of the list below
                        if not client.is_alive():
                            try:
                                self.secop_servers.remove(client)
                                client.join()
                            except Exception as err:
                                logging.error(f"Error in socket client thread: {err}")
            except OSError as err:
                logging.error(f"{err}, retrying in 10s...")
                sleep(10)
            except KeyboardInterrupt as err:
                self.stop()


    def stop(self):
        logging.warning("Interrupt signal!")
        logging.warning("Stopping SECoP servers")
        for server in self.secop_servers:
            server.socket.close()
        sys.exit(0)


    def on_mqtt_structure_message(self, client, userdata, message):
        """
        Callback for when a new structure report is received over MQTT.
        The function parses the structure into a SecNode object with
        associated Module and Accessible objects. The new node replaces
        any existing nodes - this gateway only supports a single node currently.
        """
        prefix = message.topic.split('/')[0]
        logging.info(f"New structure message received: {prefix}")
        # FIXME: Check if node already exists. If it does unsubscribe to all topics and unload the node first.
        try:
            payload = json.loads(message.payload)
        except Exception as err:
            logging.error("Invalid JSON data encountered in SECoP structure report: " + str(err))
            return
        try:
            payload['id'] = payload['equipment_id']
            if self.node and payload['id'] == self.node.id:
                self.node.deserialize(payload)
                logging.info(f"Node updated: {self.node.full_name}")
            else:
                self.node = SecNode(payload)
                logging.info(f"New node: {self.node.full_name}")
        except Exception as err:
            logging.error("Error parsing SECoP structure: " + str(err))
            return
        logging.debug(f"New node loaded with equipment id: {self.node.equipment_id}")
        # Setup callbacks on accessibles for data flow between MQTT and Epics
        logging.info("Subscribing to all value_topics for node")
        for accessible in self.node.accessibles.values():
            # Subscribe to value-topic
            try:
                self.subscribe(accessible.value_topic, self.on_mqtt_data_message)
                logging.debug(f"Subscribed to '{accessible.value_topic}'")
            except Exception as err:
                logging.error(f"Failed subscribing to value topic '{accessible.value_topic}': " + str(err))


    def on_mqtt_data_message(self, client, userdata, message):
        """
        Callback for when new data messages for an Accessible is received over
        MQTT on a value_topic.
        """
        try:
            value = json.loads(message.payload)
        except Exception as err:
            logging.warning(f"Failed parsing JSON data for accessible update on topic '{message.topic}'")
            return
        for accessible in self.node.value_topics[message.topic]:
            try:
                accessible.source_value = value
            except Exception as err:
                logging.warning(f"Failed updating value for accessible '{accessible.full_id}': {err}")
            # Inform the SECoP servers of the value change
            for server in self.secop_servers:
                try:
                    server.on_accessible_change(accessible)
                except Exception as err:
                    logging.error(f"Error sending new value for '{accessible.full_id}' to SECoP client: {err}")


    def on_socket_set_message(self, accessible, msg):
        """
        Callback for set messages from the SECoP socket server.
        """
        if accessible.readonly:
            err_msg = f"Can't set value of readonly accessible '{accessible.full_id}'"
            logging.warning(err_msg)
            raise ValueError(err_msg)
        if not accessible.set_topic:
            err_msg = f"Can't set value of accessible '{accessible.full_id}' since it has not set-topic"
            logging.warning(err_msg)
            raise ValueError(err_msg)
        try:
            set_value = json.loads(msg)
        except secop_errors.SecopException as err:
            err_msg = f"Error parsing value as JSON: {err}"
            logging.warning(err_msg)
            raise ValueError(err_msg)
        try:
            validated_value = accessible.set_value = set_value
        except Exception as err:
            err_msg = f"Can't set value of accessible '{accessible.full_id}': {err}"
            logging.warning(err_msg)
            raise ValueError(err_msg)
        payload = json.dumps(validated_value)
        self.publish(accessible.set_topic, payload)
        return accessible.set_value


    def on_socket_get_message(self, accessible):
        if accessible.get_topic:
            self.publish(accessible.get_topic, None)
            #sleep(1)  # Allow some time for the hardware to get new data. This is probably not long enough for most hardware, but it's better than nothing.


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="localhost")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=1883)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    config = {
        'mqtt': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix

    gw = SECoPECSGateway(config)
    gw.start()
